const isValidJSON = (jsonString) => {
    try {
      JSON.parse(jsonString);
      return true;
    } catch (error) {
      return false;
    }
  };
  
  console.log(isValidJSON('{"a": 2}')); 
  console.log(isValidJSON('{"a: 2')); 





  const greeting = ({ name, surname, age }) => {
    return `Hello, my name is ${name} ${surname} and I am ${age} years old.`;
  };
  
 
  console.log(greeting({ name: 'Bill', surname: 'Jacobson', age: 33 }));
  
  
  console.log(greeting({ name: 'Jim', surname: 'Power', age: 11 }));
  




const unique = (arr) => [...new Set(arr)];


console.log(unique([1, 1, 2, 3, 5, 4, 5]));




function* generator(legoBlocks) {
    for (let block of legoBlocks) {
      yield block;
    }
  }
  
  
  const it = generator(['brick', 'plate', 'minifigure', 'slope']);
  
  console.log(it.next().value); 
  console.log(it.next().value); 
